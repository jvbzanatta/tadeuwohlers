﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VibbraPlaces.Web.Data.Context;
using VibbraPlaces.Web.Models.Home;

namespace VibbraPlaces.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var dbContext = new ApplicationDbContext();

            var placeTypes = dbContext.PlaceTypes.OrderBy(p => p.Name).ToList();
            placeTypes.Insert(0, new Data.Model.PlaceType() { Id = 0, Name = "Todos" });

            var priceRating = new[]
            {
                new SelectListItem { Value = "0", Text = "Todos" },
                new SelectListItem { Value = "1", Text = "$" },
                new SelectListItem { Value = "2", Text = "$$" },
                new SelectListItem { Value = "3", Text = "$$$" },
                new SelectListItem { Value = "4", Text = "$$$$" },
                new SelectListItem { Value = "5", Text = "$$$$$" },

            };

            ViewBag.PlaceTypes = placeTypes;
            ViewBag.PriceRating = priceRating;

            var places = dbContext.Places.ToList();

            var findVM = new FindVM();
            foreach (var place in places)
            {
                findVM.Places.Add(new PlaceVM()
                {
                    Id = place.Id,
                    Name = place.Name,
                    Type = place.PlaceType.Name,
                    City = place.City,
                    State = place.State,
                    EvaluationQuantity = place.PlaceEvaluations.Count()
                });
            }

            return View(findVM);
        }

        [HttpPost]
        public ActionResult Index(FindVM model)
        {
            var dbContext = new ApplicationDbContext();

            var placeTypes = dbContext.PlaceTypes.OrderBy(p => p.Name).ToList();
            placeTypes.Insert(0, new Data.Model.PlaceType() { Id = 0, Name = "Todos" });

            var priceRating = new[]
            {
                new SelectListItem { Value = "0", Text = "Todos" },
                new SelectListItem { Value = "1", Text = "$" },
                new SelectListItem { Value = "2", Text = "$$" },
                new SelectListItem { Value = "3", Text = "$$$" },
                new SelectListItem { Value = "4", Text = "$$$$" },
                new SelectListItem { Value = "5", Text = "$$$$$" },

            };

            ViewBag.PlaceTypes = placeTypes;
            ViewBag.PriceRating = priceRating;

            if (model.Term == null)
                model.Term = string.Empty;

            var places = dbContext.Places
                .Where(p => (
                p.Name.Contains(model.Term) ||
                p.City.Contains(model.Term))
                && (model.PlaceType == 0 || p.PlaceTypeId == model.PlaceType))
                .ToList();

            foreach (var place in places)
            {
                model.Places.Add(new PlaceVM()
                {
                    Id = place.Id,
                    Name = place.Name,
                    Type = place.PlaceType.Name,
                    City = place.City,
                    State = place.State,
                    EvaluationQuantity = place.PlaceEvaluations.Count()
                });
            }

            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}