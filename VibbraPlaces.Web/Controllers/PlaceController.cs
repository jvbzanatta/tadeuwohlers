﻿using Microsoft.AspNet.Identity;
using System;
using System.Linq;
using System.Web.Mvc;
using VibbraPlaces.Web.Data.Context;
using VibbraPlaces.Web.Data.Model;
using VibbraPlaces.Web.Models.Place;

namespace VibbraPlaces.Web.Controllers
{
    [Authorize]
    public class PlaceController : Controller
    {
        private ApplicationDbContext dbContext = new ApplicationDbContext();

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.placeTypes = dbContext.PlaceTypes.OrderBy(p => p.Name).ToList();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreatePlaceVM model)
        {
            ViewBag.placeTypes = dbContext.PlaceTypes.OrderBy(p => p.Name).ToList();

            if (!ModelState.IsValid)
                return View(model);

            try
            {
                var place = new Place()
                {
                    Name = model.Name,
                    Address = model.Address,
                    City = model.City,
                    State = model.State,
                    Country = model.Country,
                    PlaceTypeId = model.PlaceTypeId,
                    InsertDate = DateTime.Now,
                    ApplicationUserId = User.Identity.GetUserId<long>()
                };

                dbContext.Places.Add(place);
                dbContext.SaveChanges();

                TempData["SuccessMessage"] = "Estabelecimento cadastrado com sucesso";
                return RedirectToAction("evaluate", "place", new { id = place.Id });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult Evaluate(long id)
        {
            var userId = User.Identity.GetUserId<long>();
            var placeEvaluation = dbContext.PlaceEvaluations.Where(p => p.PlaceId == id && p.ApplicationUserId == userId).FirstOrDefault();

            if (placeEvaluation == null)
            {
                var place = dbContext.Places.Where(p => p.Id == id).FirstOrDefault();

                if (place == null)
                    return HttpNotFound();

                var evaluatePlaceVM = new EvaluatePlaceVM()
                {
                    PlaceId = place.Id,
                    Name = place.Name,
                    Address = place.Address,
                    City = place.City,
                    Country = place.Country,
                    State = place.State,
                    PlaceType = place.PlaceType.Name
                };

                return View(evaluatePlaceVM);
            }
            else
            {
                var evaluatePlaceVM = new EvaluatePlaceVM()
                {
                    PlaceId = placeEvaluation.Place.Id,
                    Name = placeEvaluation.Place.Name,
                    Address = placeEvaluation.Place.Address,
                    City = placeEvaluation.Place.City,
                    Country = placeEvaluation.Place.Country,
                    State = placeEvaluation.Place.State,
                    PlaceType = placeEvaluation.Place.PlaceType.Name,

                    EvaluateId = placeEvaluation.Id,
                    HasInternet = placeEvaluation.HasInternet,
                    InternetSpeed = placeEvaluation.InternetSpeed,
                    OpenInternet = placeEvaluation.OpenInternet,
                    InternetPassword = placeEvaluation.InternetPassword,
                    Food = placeEvaluation.Food,
                    Drink = placeEvaluation.Drink,
                    ServiceRating = placeEvaluation.ServiceRating,
                    PriceRating = placeEvaluation.PriceRating,
                    ComfortRating = placeEvaluation.ComfortRating,
                    NoiseRating = placeEvaluation.NoiseRating,
                    GeneralEvaluationRating = placeEvaluation.GeneralEvaluationRating
                };

                return View(evaluatePlaceVM);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Evaluate(EvaluatePlaceVM model)
        {
            var place = dbContext.Places.Where(p => p.Id == model.PlaceId).FirstOrDefault();
            model.Name = place.Name;
            model.Address = place.Address;
            model.City = place.City;
            model.Country = place.Country;
            model.State = place.State;
            model.PlaceType = place.PlaceType.Name;

            if (!ModelState.IsValid)
                return View(model);

            try
            {
                if (model.EvaluateId.Equals(0))
                {
                    var placeEvaluation = new PlaceEvaluation()
                    {
                        PlaceId = model.PlaceId,
                        HasInternet = model.HasInternet,
                        InternetSpeed = model.InternetSpeed,
                        OpenInternet = model.OpenInternet,
                        InternetPassword = model.InternetPassword,
                        Food = model.Food,
                        Drink = model.Drink,
                        ServiceRating = model.ServiceRating,
                        PriceRating = model.PriceRating,
                        ComfortRating = model.ComfortRating,
                        NoiseRating = model.NoiseRating,
                        GeneralEvaluationRating = model.GeneralEvaluationRating,
                        InsertDate = DateTime.Now,
                        ApplicationUserId = User.Identity.GetUserId<long>()
                    };

                    dbContext.PlaceEvaluations.Add(placeEvaluation);
                    dbContext.SaveChanges();

                    TempData["SuccessMessage"] = "Avaliação cadastrada com sucesso";
                    return RedirectToAction("mine", "evaluation");
                }
                else
                {
                    var placeEvaluation = dbContext.PlaceEvaluations.Where(p => p.Id == model.EvaluateId).FirstOrDefault();

                    placeEvaluation.HasInternet = model.HasInternet;
                    placeEvaluation.InternetPassword = model.InternetSpeed;
                    placeEvaluation.OpenInternet = model.OpenInternet;
                    placeEvaluation.InternetPassword = model.InternetPassword;
                    placeEvaluation.Food = model.Food;
                    placeEvaluation.Drink = model.Drink;
                    placeEvaluation.ServiceRating = model.ServiceRating;
                    placeEvaluation.PriceRating = model.PriceRating;
                    placeEvaluation.ComfortRating = model.ComfortRating;
                    placeEvaluation.NoiseRating = model.NoiseRating;
                    placeEvaluation.GeneralEvaluationRating = model.GeneralEvaluationRating;
                    placeEvaluation.UpdateDate = DateTime.Now;

                    dbContext.SaveChanges();

                    TempData["SuccessMessage"] = "Avaliação atualizada com sucesso";
                    return RedirectToAction("mine", "evaluation");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex.Message);
                return View(model);
            }
        }
                
        [HttpGet]
        [AllowAnonymous]
        public ActionResult View(long id)
        {
            var place = dbContext.Places.Where(p => p.Id == id).FirstOrDefault();

            if (place == null)
                return HttpNotFound();

            return View(place);
        }

        [HttpGet]
        [AllowAnonymous]
        [ActionName("view-evaluation")]
        public ActionResult ViewEvaluation(long id)
        {
            var evaluation = dbContext.PlaceEvaluations.Where(p => p.Id == id).FirstOrDefault();

            if (evaluation == null)
                return HttpNotFound();

            return View(evaluation);
        }
    }
}