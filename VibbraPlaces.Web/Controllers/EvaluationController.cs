﻿using Microsoft.AspNet.Identity;
using System.Linq;
using System.Web.Mvc;
using VibbraPlaces.Web.Data.Context;
using VibbraPlaces.Web.Models.Evaluation;

namespace VibbraPlaces.Web.Controllers
{
    public class EvaluationController : Controller
    {
        private ApplicationDbContext dbContext = new ApplicationDbContext();

        public ActionResult Index()
        {
            var dbContext = new ApplicationDbContext();

            var placeTypes = dbContext.PlaceTypes.OrderBy(p => p.Name).ToList();
            placeTypes.Insert(0, new Data.Model.PlaceType() { Id = 0, Name = "Todos" });

            var priceRating = new[]
            {
                new SelectListItem { Value = "0", Text = "Todos" },
                new SelectListItem { Value = "1", Text = "$" },
                new SelectListItem { Value = "2", Text = "$$" },
                new SelectListItem { Value = "3", Text = "$$$" },
                new SelectListItem { Value = "4", Text = "$$$$" },
                new SelectListItem { Value = "5", Text = "$$$$$" },

            };

            ViewBag.PlaceTypes = placeTypes;
            ViewBag.PriceRating = priceRating;

            var evaluations = dbContext.PlaceEvaluations.ToList();

            var findVM = new FindEvaluationVM();
            foreach (var evaluation in evaluations)
            {
                findVM.Evaluations.Add(new EvaluationVM()
                {
                    Id = evaluation.Id,
                    PlaceId = evaluation.PlaceId,
                    Name = evaluation.Place.Name,
                    Type = evaluation.Place.PlaceType.Name,
                    City = evaluation.Place.City,
                    State = evaluation.Place.State,
                    PriceRating = evaluation.PriceRating,
                    GeneralRating = evaluation.GeneralEvaluationRating,
                    HasInternet = evaluation.HasInternet
                });
            }

            return View(findVM);
        }

        [HttpPost]
        public ActionResult Index(FindEvaluationVM model)
        {
            var dbContext = new ApplicationDbContext();

            var placeTypes = dbContext.PlaceTypes.OrderBy(p => p.Name).ToList();
            placeTypes.Insert(0, new Data.Model.PlaceType() { Id = 0, Name = "Todos" });

            var priceRating = new[]
            {
                new SelectListItem { Value = "0", Text = "Todos" },
                new SelectListItem { Value = "1", Text = "$" },
                new SelectListItem { Value = "2", Text = "$$" },
                new SelectListItem { Value = "3", Text = "$$$" },
                new SelectListItem { Value = "4", Text = "$$$$" },
                new SelectListItem { Value = "5", Text = "$$$$$" },

            };

            ViewBag.PlaceTypes = placeTypes;
            ViewBag.PriceRating = priceRating;

            if (model.Term == null)
                model.Term = string.Empty;

            var evaluations = dbContext.PlaceEvaluations
                .Where(p => (
                p.Place.Name.Contains(model.Term) ||
                p.Place.City.Contains(model.Term))
                && (model.PlaceType == 0 || p.Place.PlaceTypeId == model.PlaceType)
                && (model.Price == 0 || p.PriceRating == model.Price)
                )
                .ToList();

            foreach (var evaluation in evaluations)
            {
                model.Evaluations.Add(new EvaluationVM()
                {
                    Id = evaluation.Id,
                    PlaceId = evaluation.PlaceId,
                    Name = evaluation.Place.Name,
                    Type = evaluation.Place.PlaceType.Name,
                    City = evaluation.Place.City,
                    State = evaluation.Place.State,
                    PriceRating = evaluation.PriceRating,
                    GeneralRating = evaluation.GeneralEvaluationRating,
                    HasInternet = evaluation.HasInternet
                });
            }

            return View(model);
        }

        [Authorize]
        public ActionResult Mine()
        {
            var dbContext = new ApplicationDbContext();

            var userId = User.Identity.GetUserId<long>();
            var evaluations = dbContext.PlaceEvaluations.Where(e => e.ApplicationUserId == userId).ToList();

            var findVM = new FindEvaluationVM();
            foreach (var evaluation in evaluations)
            {
                findVM.Evaluations.Add(new EvaluationVM()
                {
                    Id = evaluation.Id,
                    PlaceId = evaluation.PlaceId,
                    Name = evaluation.Place.Name,
                    Type = evaluation.Place.PlaceType.Name,
                    City = evaluation.Place.City,
                    State = evaluation.Place.State,
                    PriceRating = evaluation.PriceRating,
                    GeneralRating = evaluation.GeneralEvaluationRating,
                    HasInternet = evaluation.HasInternet
                });
            }

            return View(findVM);
        }
    }
}