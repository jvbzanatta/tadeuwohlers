﻿(function ($) {
    function Rating() {
        var $this = this;
        function initialize() {
            $(".star").click(function () {
                //$(".star").removeClass('active');
                console.log($(this));
                $(this).parent().find('.star').removeClass('active');
                $(this).addClass('active');
                var starValue = $(this).data("value");

                var elementName = $(this).parent().attr('data-name');

                $("#" + elementName).val(starValue);
            });
        }
        $this.init = function () {
            initialize();
        };
    }

    function Dollar() {
        var $this = this;
        function initialize() {
            $(".star").click(function () {
                //$(".star").removeClass('active');
                console.log($(this));
                $(this).parent().find('.star').removeClass('active');
                $(this).addClass('active');
                var starValue = $(this).data("value");

                var elementName = $(this).parent().attr('data-name');

                $("#" + elementName).val(starValue);
            });
        }
        $this.init = function () {
            initialize();
        };
    }

    $(function () {
        var self = new Rating();
        self.init();

        self = new Dollar();
        self.init();
    });

}(jQuery))