﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using VibbraPlaces.Web.Data.Model;

namespace VibbraPlaces.Web.Data.Context
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, CustomRole, long, CustomUserLogin, CustomUserRole, CustomUserClaim>
    {
        public ApplicationDbContext()
            : base("VibbraPlacesDB")
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //Set default string type in database
            modelBuilder.Properties<string>()
                .Configure(s => s.HasMaxLength(200).HasColumnType("varchar"));

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            //Identity tables
            modelBuilder.Entity<ApplicationUser>().ToTable("User");
            modelBuilder.Entity<CustomRole>().ToTable("Role");
            modelBuilder.Entity<CustomUserRole>().ToTable("UserRoles");
            modelBuilder.Entity<CustomUserLogin>().ToTable("UserLogins");
            modelBuilder.Entity<CustomUserClaim>().ToTable("UserClaims");

            //Busienss tables
            modelBuilder.Entity<PlaceType>().ToTable("PlaceType");
            modelBuilder.Entity<Place>().ToTable("Place");
            modelBuilder.Entity<PlaceEvaluation>().ToTable("PlaceEvaluation");
        }

        public DbSet<PlaceType> PlaceTypes { get; set; }
        public DbSet<Place> Places { get; set; }
        public DbSet<PlaceEvaluation> PlaceEvaluations { get; set; }
    }
}