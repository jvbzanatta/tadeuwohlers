﻿using System;
using System.Collections.Generic;

namespace VibbraPlaces.Web.Data.Model
{
    public class Place
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public long PlaceTypeId { get; set; }
        public DateTime InsertDate { get; set; }
        public long ApplicationUserId { get; set; }

        public virtual PlaceType PlaceType { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }

        public virtual ICollection<PlaceEvaluation> PlaceEvaluations { get; set; }
    }
}