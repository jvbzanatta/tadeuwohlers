﻿using System.Collections;
using System.Collections.Generic;

namespace VibbraPlaces.Web.Data.Model
{
    public class PlaceType
    {
        public long Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Place> Places { get; set; }
    }
}