﻿using System;

namespace VibbraPlaces.Web.Data.Model
{
    public class PlaceEvaluation
    {
        public long Id { get; set; }
        public long PlaceId { get; set; }
        public bool HasInternet { get; set; }
        public string InternetSpeed { get; set; }
        public bool OpenInternet { get; set; }
        public string InternetPassword { get; set; }
        public string Food { get; set; }
        public string Drink { get; set; }
        public int ServiceRating { get; set; }
        public int PriceRating { get; set; }
        public int ComfortRating { get; set; }
        public int NoiseRating { get; set; }
        public int GeneralEvaluationRating { get; set; }
        public DateTime InsertDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public long ApplicationUserId { get; set; }

        public virtual Place Place { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}