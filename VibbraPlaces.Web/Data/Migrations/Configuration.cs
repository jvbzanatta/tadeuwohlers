namespace VibbraPlaces.Web.Migrations
{
    using Microsoft.AspNet.Identity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using VibbraPlaces.Web.Data.Model;

    internal sealed class Configuration : DbMigrationsConfiguration<VibbraPlaces.Web.Data.Context.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "VibbraPlaces.Web.Data.Context.ApplicationDbContext";
        }

        protected override void Seed(VibbraPlaces.Web.Data.Context.ApplicationDbContext context)
        {

            if (!context.Users.Any(u => u.UserName == "srvibbraneo@gmail.com"))
            {
                var userVibbraneo = new ApplicationUser()
                {
                    Email = "srvibbraneo@gmail.com",
                    UserName = "srvibbraneo@gmail.com"
                };

                var manager = new ApplicationUserManager(new CustomUserStore(context));
                manager.Create(userVibbraneo, "vibbra@123");
            }

            context.PlaceTypes.AddOrUpdate(
                p => p.Name,
                new PlaceType { Name = "Caf�" },
                new PlaceType { Name = "Restaurante" },
                new PlaceType { Name = "Coworking" },
                new PlaceType { Name = "Livraria" },
                new PlaceType { Name = "Outro" }
            );
        }
    }
}
