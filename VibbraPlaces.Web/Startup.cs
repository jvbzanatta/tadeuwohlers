﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VibbraPlaces.Web.Startup))]
namespace VibbraPlaces.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
