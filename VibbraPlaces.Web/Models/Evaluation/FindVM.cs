﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VibbraPlaces.Web.Models.Evaluation
{
    public class FindEvaluationVM
    {
        public FindEvaluationVM()
        {
            Evaluations = new List<EvaluationVM>();
        }

        [Display(Name = "Nome ou endereço")]
        public string Term { get; set; }

        [Display(Name = "Tipo de Local")]
        public int PlaceType { get; set; }

        [Display(Name = "Valor")]
        public int Price { get; set; }

        public List<EvaluationVM> Evaluations { get; set; }
    }

    public class EvaluationVM
    {
        public long Id { get; set; }
        public long PlaceId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int PriceRating { get; set; }
        public int GeneralRating { get; set; }
        public bool HasInternet { get; set; }
    }
}