﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VibbraPlaces.Web.Models.Home
{
    public class FindVM
    {
        public FindVM()
        {
            Places = new List<PlaceVM>();
        }

        [Display(Name = "Nome ou endereço")]
        public string Term { get; set; }

        [Display(Name = "Tipo de Local")]
        public int PlaceType { get; set; }

        [Display(Name = "Valor")]
        public int Price { get; set; }

        public List<PlaceVM> Places { get; set; }
    }

    public class PlaceVM
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int EvaluationQuantity { get; set; }
    }
}