﻿using System.ComponentModel.DataAnnotations;

namespace VibbraPlaces.Web.Models.Place
{
    public class EvaluatePlaceVM
    {
        public long PlaceId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PlaceType { get; set; }

        public long EvaluateId { get; set; }

        [Required]
        [Display(Name = "Possui internet?")]
        public bool HasInternet { get; set; }

        [Display(Name = "Velocidade da internet")]
        public string InternetSpeed { get; set; }

        [Required]
        [Display(Name = "Internet aberta?")]
        public bool OpenInternet { get; set; }

        [Display(Name = "Senha da internet")]
        public string InternetPassword { get; set; }

        [Required]
        [Display(Name = "Tipos de comida")]
        public string Food { get; set; }

        [Required]
        [Display(Name = "Tipos de bebida")]
        public string Drink { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Por favor preencha uma avaliação para o campo {0}")]
        [Display(Name = "Atendimento")]
        public int ServiceRating { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Por favor preencha uma avaliação para o campo {0}")]
        [Display(Name = "Preço")]
        public int PriceRating { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Por favor preencha uma avaliação para o campo {0}")]
        [Display(Name = "Conforto")]
        public int ComfortRating { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Por favor preencha uma avaliação para o campo {0}")]
        [Display(Name = "Ruído (Barulho)")]
        public int NoiseRating { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Por favor preencha uma avaliação para o campo {0}")]
        [Display(Name = "Avaliação Geral")]
        public int GeneralEvaluationRating { get; set; }
    }
}