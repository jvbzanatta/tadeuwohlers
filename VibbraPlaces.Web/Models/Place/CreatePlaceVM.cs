﻿using System.ComponentModel.DataAnnotations;

namespace VibbraPlaces.Web.Models.Place
{
    public class CreatePlaceVM
    {
        [Required]
        [Display(Name = "Nome")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Endereço")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "Cidade")]
        public string City { get; set; }

        [Required]
        [Display(Name = "Estado")]
        public string State { get; set; }

        [Required]
        [Display(Name = "País")]
        public string Country { get; set; }

        [Required]
        [Display(Name = "Tipo de Estabelecimento")]
        public long PlaceTypeId { get; set; }
    }
}