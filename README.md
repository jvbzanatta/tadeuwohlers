Entrega do projeto VibbraPlaces.

Link para testar o projeto: http://twohlers-001-site5.htempurl.com

Detalhes da entrega:

1) Avaliação do escopo:
O escopo ficou claro. 

2) Estimativa em horas do desenvolvimento:
18 horas.
Detalhes sobre a estimativa estão na planilha "Controle de horas.xlsx", dentro da pasta "Planilha de horas" do repositório.

3) Estimativa em dias do prazo de entrega:
7 dias, ou seja, entrega até o dia 01/03/2019.

4) Aponte as horas e atividade que utilizou para fazer o teste:
Horas apontadas na planilha "Controle de horas.xlsx".

5) Detalhes do desenvolvimento:

Todos os itens foram desenvolvidos.

O projeto foi criado em asp.net mvc, utilizando o Visual Studio 2017 Community.
Foi utilizado o ORM Entity Framework para comunicação com o banco de dados.
O banco de dados utilizado é o SQL Server 2016.
Foi utilizado Identity Framework para gestão de logins.

No ambiente de testes disponbilizado não é possível efetuar login através do Facebook ou Google pois o site não possui SSL.
No entando, rodando local no Visual Studio é possível efetuar login através do Facebook ou Google.

Para o layout do projeto foram utilizado elmentos básicos do bootstrap.